﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafAttributes : MonoBehaviour
{
    public int rightLeft;
    public GameObject leafMeshHolder;
    public float leafScale;

    bool fulfilled;
    public float dummyScale;

    public int indexHex;

    public Transform facingTarget;
    public bool faceToTarget;

    Quaternion targetRot;
    Quaternion myRot;

    public GameObject leafParent;
    public GameObject orange;

    public bool FallenState;
    public ParticleSystem CoinParticle;

    // Start is called before the first frame update
    void Start()
    {
        if(rightLeft == 1)
        {
            leafMeshHolder.transform.localRotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            leafMeshHolder.transform.localRotation = Quaternion.Euler(0, 0, -90);
        }

        leafMeshHolder.transform.localScale = Vector3.zero;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(dummyScale < leafScale && !fulfilled)
        {
            dummyScale += Time.deltaTime;
        }
        else
        {
            fulfilled = true;
            dummyScale = leafScale;
        }

        leafMeshHolder.transform.localScale = new Vector3(dummyScale, dummyScale, dummyScale);

        if (!FallenState)
        {
            if (faceToTarget && facingTarget)
            {
                //targetRot =( Quaternion.FromToRotation(leafMeshHolder.transform.right, facingTarget.position - transform.position));
                //
                float Angle = Vector3.SignedAngle(transform.up, facingTarget.position - transform.position, Vector3.forward);
                //Debug.Log(Angle);

                targetRot = Quaternion.Euler(0, 0, Angle);

            }
            else
            {
                targetRot = myRot;
            }

            leafMeshHolder.transform.localRotation = Quaternion.Lerp(leafMeshHolder.transform.localRotation, targetRot, Time.deltaTime * 10);

            if (!Input.GetMouseButton(0))
            {
                faceToTarget = false;
            }
        }
        
        //transform.rotation = Quaternion.FromToRotation(transform.right, facingTarget.position - transform.position);
    }

    public void GetFacingTarget(Transform t)
    {
        facingTarget = t;
        faceToTarget = true;
    }

    public void GetMyLocalRot()
    {
        myRot = leafMeshHolder.transform.localRotation;
    }

    public void FallDown()
    {
        FallenState = true;
        this.gameObject.AddComponent<Rigidbody>();
        this.transform.SetParent(RaycastingSystem.instance.gameObject.transform);
        this.gameObject.GetComponent<Rigidbody>().AddTorque(10, 10, 10);
        this.gameObject.GetComponent<Rigidbody>().AddForce(0, -500, 0);

        Invoke("PlayCoinParticle", 2f);

        
    }


    public void PlayCoinParticle()
    {
        GameObject go = Instantiate(CoinParticle.gameObject, orange.transform.position+ new Vector3(0,1,0), Quaternion.identity);
        Destroy(go, 2f);
        this.gameObject.SetActive(false);
    }
}
