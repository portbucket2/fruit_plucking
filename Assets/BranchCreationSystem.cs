﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BranchCreationSystem : MonoBehaviour
{
    public Transform[] BranchStations;
    public GameObject HexUnit;
    public HexUnitAttributes[] HexUnitsAttr;
    public GameObject HexUnitsHolder;

    public int[] tempTar;

    public int LastrunningHexIndexes;
    public BranchMeshCreation branchMeshCreation;
    public BranchStationsManager BranchStationsManager;
    public float currentHexScale;

    public Transform[] subBranchpoints;
    public int[] subBranchTriggerpoints;
    public bool subBranching;
    public int subBranchIndex;
    public GameObject myClone;
    public GameObject subBone;
    public bool BoneMode;

    public float[] leafHolderHexIndexes;
    public GameObject leafMesh;
    public List<GameObject> leafObjects;

    public float[] leafBranchHolderHexIndexes;
    public GameObject leafBranchMesh;
    public List<GameObject> leafBranchObjects;

    int leafRightSide;
    public bool GenerateFruits;
    public bool GenerateLeafBranch;
    public bool flipBranches;
    //public int HexesGotDest;

    public float branchCreationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        if (subBranching)
        {
            BranchesManage.instance.AddMeToBranchesList(this.gameObject);
        }
        
        InitiateBranch();
        tempTar = new int[BranchStations.Length];
        LastrunningHexIndexes = BranchStations.Length - 1;

        branchMeshCreation.InitiateBranchMesh();
    }

    // Update is called once per frame
    void Update()
    {
        if (!BoneMode)
        {
            for (int i = BranchStations.Length - 1; i > 0; i--)
            {
                MoveTheHex(i);
            }
        }
        

    }

    public void InitiateBranch()
    {
        branchMeshCreation.HexUnits = HexUnitsAttr = new HexUnitAttributes[BranchStations.Length];
        
        for (int i = 0; i < BranchStations.Length; i++)
        {
            GameObject Go = Instantiate(HexUnit, BranchStations[0].position, BranchStations[0].rotation);
            Go.transform.SetParent(HexUnitsHolder.transform);
            branchMeshCreation.HexUnits[i] = HexUnitsAttr[i] = Go.GetComponent<HexUnitAttributes>();
            Go.GetComponent<HexUnitAttributes>().BranchCreationSystem = this;
            HexUnitsAttr[i].targetStation = i;

            
            HexUnitsAttr[i].affectingBone0 = BranchStationsManager.bones[(int)((float)(HexUnitsAttr[i].targetStation ) / (float)BranchStationsManager.subSteps)];
            HexUnitsAttr[i].subBone0 = Instantiate(subBone, BranchStationsManager.subStationsList[i].transform.position, BranchStationsManager.subStationsList[i].transform.rotation);
            HexUnitsAttr[i].subBone0.transform.SetParent(HexUnitsAttr[i].affectingBone0.transform);
            if (BranchStationsManager.bones[((int)((float)(HexUnitsAttr[i].targetStation) / (float)BranchStationsManager.subSteps)) + 1])
            {
                HexUnitsAttr[i].affectingBone1 = BranchStationsManager.bones[((int)((float)(HexUnitsAttr[i].targetStation) / (float)BranchStationsManager.subSteps)) + 1];
                HexUnitsAttr[i].boneWeight = ((float)(HexUnitsAttr[i].targetStation) / (float)BranchStationsManager.subSteps) - ((int)((float)(HexUnitsAttr[i].targetStation) / (float)BranchStationsManager.subSteps));

                HexUnitsAttr[i].subBone1 = Instantiate(subBone, BranchStationsManager.subStationsList[i].transform.position, BranchStationsManager.subStationsList[i].transform.rotation);
                HexUnitsAttr[i].subBone1.transform.SetParent(HexUnitsAttr[i].affectingBone1.transform);
            }
            else
            {
                HexUnitsAttr[i].boneWeight = 0;
            }

            


            //currentHexScale = currentHexScale * (1 - ((float)i / (BranchStations.Length - 1)));
            HexUnitsAttr[i].sizeScale = currentHexScale * (1 - ((float)i / (BranchStations.Length - 1)));
            //HexUnitsAttr[i].sizeScale = HexUnitsAttr[i].sizeScale * (1 - ((float)i / (BranchStations.Length - 1)));

        }
    }

    public void MoveTheHex(int i)
    {
        if (i >= LastrunningHexIndexes)
        {
            HexUnitsAttr[i].activated = true;
            if(i== 1)
            {
                HexUnitsAttr[0].activated = true;
            }
            HexUnitsAttr[i].gameObject.transform.position = Vector3.MoveTowards(HexUnitsAttr[i].gameObject.transform.position, BranchStations[tempTar[i]].position, Time.deltaTime * branchCreationSpeed);
            HexUnitsAttr[i].tempTargetStation = tempTar[i];
            HexUnitsAttr[i].gameObject.transform.rotation = Quaternion.RotateTowards(HexUnitsAttr[i].gameObject.transform.rotation, BranchStations[tempTar[i]].rotation, Time.deltaTime * branchCreationSpeed * Mathf.Rad2Deg);
            if (HexUnitsAttr[i].gameObject.transform.position == BranchStations[tempTar[i]].position)
            {
                if (tempTar[i] < HexUnitsAttr[i].targetStation)
                {

                    if (tempTar[i] == 1)
                    {
                        LastrunningHexIndexes -= 1;

                    }
                    tempTar[i] += 1;

                    if ((i == BranchStations.Length - 1))
                    {
                        if (subBranching)
                        {
                            int a =(int) (BranchStations.Length / (BranchStationsManager.bones.Length - 1));
                            for (int b = 0; b < (BranchStationsManager.bones.Length - 1); b++)
                            {
                                if (tempTar[i] == ((1+b)*a) + (a / 2))
                                {
                                    
                                    float neg = b % 2;
                                    if(neg == 0)
                                    {
                                        neg = 1;
                                    }
                                    else
                                    {
                                        neg = -1;
                                    }
                                    float frac = 1 / (float)(BranchStationsManager.bones.Length - 1);
                                    float scale = 1 - (b+1) * frac;
                                    if (flipBranches)
                                    {
                                        neg *= -1;
                                    }
                                    BranchesManage.instance.BranchInstantiatingAtDirection(0, -40* neg, BranchStationsManager.bones[1 + b].transform, currentHexScale , scale, subBranchIndex);
                                }
                            }

                            if (tempTar[i] == a + (a/2))
                            {
                                //GameObject go = Instantiate(myClone, subBranchpoints[0].position, subBranchpoints[0].rotation);
                                //go.GetComponent<BranchCreationSystem>().currentHexScale *= 0.5f;
                                //go.transform.localScale = Vector3.one * 0.66f;
                                //go.transform.SetParent(BranchStationsManager.bones[0].transform);
                                //
                                //BranchesManage.instance.AddMeToBranchesList(go.gameObject);

                                //BranchesManage.instance.BranchInstantiatingAtDirection(0, -20, BranchStationsManager.bones[1].transform);
                            }
                            if (tempTar[i] == (2*a) + (a / 2))
                            {
                                //GameObject go = Instantiate(myClone, subBranchpoints[0].position, subBranchpoints[0].rotation);
                                //go.GetComponent<BranchCreationSystem>().currentHexScale *= 0.5f;
                                //go.transform.localScale = Vector3.one * 0.66f;
                                //go.transform.SetParent(BranchStationsManager.bones[0].transform);
                                //
                                //BranchesManage.instance.AddMeToBranchesList(go.gameObject);

                                //BranchesManage.instance.BranchInstantiatingAtDirection(0, -20, BranchStationsManager.bones[2].transform);
                            }
                            //if (tempTar[i] == subBranchTriggerpoints[1])
                            //{
                            //    GameObject go = Instantiate(myClone, subBranchpoints[1].position, subBranchpoints[1].rotation);
                            //    go.GetComponent<BranchCreationSystem>().currentHexScale *= 0.2f;
                            //    go.transform.localScale = Vector3.one * 0.33f;
                            //    go.transform.SetParent(BranchStationsManager.bones[1].transform);
                            //
                            //    BranchesManage.instance.AddMeToBranchesList(go.gameObject);
                            //}

                            
                        }

                        if (GenerateFruits && subBranchIndex>1)
                        {
                            for (int j = 0; j < leafHolderHexIndexes.Length; j++)
                            {
                                if (tempTar[i] == (int)((float)leafHolderHexIndexes[j] * (float)BranchStations.Length))
                                {
                                    GameObject go = Instantiate(leafMesh, BranchStationsManager.subStationsList[tempTar[i]].transform.position, BranchStationsManager.subStationsList[tempTar[i]].transform.rotation);
                                    go.GetComponent<LeafAttributes>().leafParent = this.gameObject;
                                    //int r = Random.Range(0, 2);
                                    if (leafRightSide == 0)
                                    {
                                        leafRightSide = 1;
                                    }
                                    else
                                    {
                                        leafRightSide = 0;
                                    }
                                    float f = (float)(tempTar[i] + 1) / (float)(BranchStations.Length);
                                    go.GetComponent<LeafAttributes>().rightLeft = leafRightSide;
                                    go.GetComponent<LeafAttributes>().leafScale = 1 - (0.5f * f);
                                    go.GetComponent<LeafAttributes>().indexHex = tempTar[i];

                                    leafObjects.Add(go);
                                }

                            }
                        }
                        if (GenerateLeafBranch)
                        {
                            for (int j = 0; j < leafBranchHolderHexIndexes.Length; j++)
                            {
                                if (tempTar[i] == (int)((float)leafBranchHolderHexIndexes[j] * (float)BranchStations.Length))
                                {
                                    GameObject go = Instantiate(leafBranchMesh, BranchStationsManager.subStationsList[tempTar[i]].transform.position, BranchStationsManager.subStationsList[tempTar[i]].transform.rotation);
                                    //go.transform.SetParent(this.transform);
                                    //go.GetComponent<LeafAttributes>().leafParent = this.gameObject;
                                    //int r = Random.Range(0, 2);
                                    //if (leafRightSide == 0)
                                    //{
                                    //    leafRightSide = 1;
                                    //}
                                    //else
                                    //{
                                    //    leafRightSide = 0;
                                    //}
                                    float f = (float)(tempTar[i] + 1) / (float)(BranchStations.Length);
                                    //go.GetComponent<LeafAttributes>().rightLeft = leafRightSide;
                                    //go.GetComponent<LeafAttributes>().leafScale = 1 - (0.5f * f);
                                    go.GetComponent<LeafBranchAttribute>().indexHex = tempTar[i];

                                    leafBranchObjects.Add(go);
                                }

                            }
                        }


                    }
                    
                }
                
            }
        }

        if(LastrunningHexIndexes <= 1)
        {
            Invoke("EnableBoneMode", 1f);
        }
        else
        {
            BoneMode = false;
        }

    }


    void EnableBoneMode()
    {
        BoneMode = true;
        ReparentLeaves();
        ReparentLeafBranches();
    }

    public void RestartGame()
    {
        Application.LoadLevel(0);
    }

    public void ReparentLeaves()
    {
        for (int i = 0; i < leafObjects.Count; i++)
        {
            leafObjects[i].transform.SetParent(HexUnitsAttr[(leafObjects[i].GetComponent<LeafAttributes>().indexHex)].transform);
            leafObjects[i].GetComponent<LeafAttributes>().GetMyLocalRot();
        }
    }
    public void ReparentLeafBranches()
    {
        for (int i = 0; i < leafBranchObjects.Count; i++)
        {
            leafBranchObjects[i].transform.SetParent(HexUnitsAttr[(leafBranchObjects[i].GetComponent<LeafBranchAttribute>().indexHex)].transform);
            //leafBranchObjects[i].GetComponent<LeafAttributes>().GetMyLocalRot();
        }
    }
}
