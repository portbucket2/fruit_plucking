﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchWaveAnimCreation : MonoBehaviour
{
    public float sinAngle;
    public float sineValue;
    public float tearSineValue;
    public float[] sineValues;
    public GameObject[] bones;
    public float animSpeed;
    public float MaxAngle;
    public float valueOffset;

    public BranchCreationSystem branchCreationSystem;

    public float intensity;
    public bool rightTension;

    public float multiplier;

    public bool released;

    public float DummySignValue;

    public bool MyLeafClicked;

    public bool GotClickError;
    public float clickError;

    public float TensionAngleBias;

    // Start is called before the first frame update
    void Start()
    {
        float up = animSpeed * 1.1f;
        float down = animSpeed * 0.9f;
        animSpeed = Random.Range(down, up);
    }

    // Update is called once per frame
    void Update()
    {


        //if (Input.GetMouseButtonDown(0))
        //{
        //    intensity = 1;
        //    sinAngle = 180;
        //}
        if (Input.GetMouseButtonDown(0))
        {
            if (released)
            {
                released = false;
            }

            tearSineValue = 0;
            
        }
        if (Input.GetMouseButton(0) && !released && RaycastingSystem.instance.currentLeafAttr && MyLeafClicked)
        {
            
            TurnSide();

            if (RaycastingSystem.instance.currentLeafAttr)
            {
                
                MeasureDistanceFromClickDirection(RaycastingSystem.instance.hitPoint, RaycastingSystem.instance.currentLeafAttr.transform);
            }
            

        }
        else
        {
            if (branchCreationSystem.BoneMode)
            {
                waveAnim();
            }
            DummySignValue = 0;
            MyLeafClicked = false;
            GotClickError = false;
            TensionAngleBias = 0;
        }

        if (Input.GetMouseButtonUp(0) &&  MyLeafClicked)
        {
            if (!released)
            {
                

                //if (rightTension)
                //{
                //    sinAngle = 0;
                //}
                //else
                //{
                //    sinAngle = 180;
                //}

                //for (int i = 0; i < sineValues.Length; i++)
                //{
                //    sineValues[i] = sineValue;
                //
                //    Quaternion rot = Quaternion.Euler(new Vector3(0, 0, sineValues[i] * MaxAngle * intensity));
                //    bones[i].transform.localRotation = Quaternion.Lerp(bones[i].transform.localRotation, rot, Time.deltaTime * 2);
                //}
            }
            
        }



    }

    public void waveAnim()
    {
        sinAngle += Time.deltaTime * animSpeed;
        if(sinAngle >= 360)
        {
            sinAngle -= 360;
        }

        //sineValue = Mathf.Sin(sinAngle* Mathf.Deg2Rad );

        for (int i = 0; i < sineValues.Length; i++)
        {
            float angle = (sinAngle + (i * valueOffset));
            if (angle >= 360)
            {
                angle -= 360;
            }
            sineValue = Mathf.Sin(angle * Mathf.Deg2Rad);
            
            sineValues[i] = sineValue ;

            Quaternion rot = Quaternion.Euler(new Vector3(0, 0, sineValues[i] * MaxAngle* intensity + TensionAngleBias));
            bones[i].transform.localRotation =Quaternion.Lerp(bones[i].transform.localRotation, rot, Time.deltaTime * 10);
        }

        //intensity -= Time.deltaTime;
        intensity = Mathf.Lerp(intensity, 0.05f, Time.deltaTime*0.5f );


    }

    public void Release(float sineAngleee , float tearValue)
    {
        //intensity = 1* Mathf.Abs(tearSineValue* 0.1f);
        //tearSineValue = tearValue;
        intensity = 1;

        sinAngle = sineAngleee ; 
        
    }
    public void DeterMineSwingIntensityAndDirection()
    {
        if (rightTension)
        {
            sinAngle = 0;
        }
        else
        {
            sinAngle = 180;
        }
        
        intensity = 1 * Mathf.Abs(tearSineValue * 0.5f);
        //intensity = 0.2f;
    }

    public void TurnSide()
    {

        intensity = 1f;
        if (((tearSineValue) >1f) )
        {

            //DestroyLeaf(RaycastingSystem.instance.currentLeafAttr.gameObject);
            RaycastingSystem.instance.currentLeafAttr.FallDown();
            DeterMineSwingIntensityAndDirection();

            for (int i = 0; i < BranchesManage.instance.BranchesOfTree.Count; i++)
            {
                BranchesManage.instance.BranchesOfTree[i].GetComponent<BranchWaveAnimCreation>().Release(sinAngle, tearSineValue);
            }
            released = true;
            RaycastingSystem.instance.currentLeafAttr = null;

            //tearSineValue = 0;

            MyLeafClicked = false;
            //released = true;

            RaycastingSystem.instance.LostLeafRef();

            Debug.Log("Released " + tearSineValue);
        }
        
        

        
        for (int i = 0; i < sineValues.Length; i++)
        {
            sineValues[i] = sineValue;
            
            Quaternion rot = Quaternion.Euler(new Vector3(0, 0, sineValues[i] * MaxAngle * intensity));
            bones[i].transform.localRotation = Quaternion.Lerp(bones[i].transform.localRotation, rot, Time.deltaTime * 2);
        }
        //sinAngle = Mathf.Asin(sineValue * Mathf.Rad2Deg);
        DeterMineSwingIntensityAndDirection();
        //intensity -= Time.deltaTime;

    }


    public void DestroyLeaf(GameObject go)
    {
        
        Destroy(go);
        Debug.Log("Leaf Gone");
        
    }


    public void MeasureDistanceFromClickDirection(Vector3 hitPoint ,Transform leafPos)
    {
        Debug.DrawLine(Vector3.zero, leafPos.position, Color.blue);
        
        Vector3 localTranslation = leafPos.transform.InverseTransformPoint(hitPoint);
        if (!GotClickError)
        {
            clickError = localTranslation.x;
            GotClickError = true;
        }
        
        float Multiplier = 1;
        if (!MyLeafClicked)
        {
            Multiplier = 0.2f;
        }
        sineValue = (localTranslation.x - (clickError)) * -0.5f * Multiplier;
        tearSineValue = Vector3.Magnitude( localTranslation)* 0.3f;
        //Debug.Log(transform.position);

        if (-sineValue > 0)
        {
            rightTension = true;
        }
        else if (-sineValue < 0)
        {
            rightTension = false;
        }

        for (int i = 0; i < BranchesManage.instance.BranchesOfTree.Count; i++)
        {
            BranchesManage.instance.BranchesOfTree[i].GetComponent<BranchWaveAnimCreation>().TensionAngleBias = 2 * sineValue;
        }
        TensionAngleBias = 0;
    }

    public void GetClickError()
    {

    }
}
