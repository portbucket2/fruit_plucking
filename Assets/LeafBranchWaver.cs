﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafBranchWaver : MonoBehaviour
{
    public float SineAngle;
    public int ValueSampleCount;
    public float incriment;
    public float[] swingValues;
    public static LeafBranchWaver instance;
    public float swingSpeed;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        swingValues = new float[ValueSampleCount];
    }

    // Update is called once per frame
    void Update()
    {
        SineAngle += Time.deltaTime* swingSpeed;
        if(SineAngle > 360)
        {
            SineAngle -= 360;
        }

        for (int i = 0; i < ValueSampleCount; i++)
        {
            float Angle = SineAngle + (i * incriment);
            if (Angle > 360)
            {
                Angle -= 360;
            }

            swingValues[i] = Mathf.Sin(Angle * Mathf.Deg2Rad);
        }
    }
}
