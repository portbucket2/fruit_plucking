﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafBranchAttribute : MonoBehaviour
{
    public GameObject leafBranchHolder;
    public float RadialAngle;
    public float targetScale;
    public int swingValueIndex;
    float scale;
    public int indexHex;
    // Start is called before the first frame update
    void Start()
    {
        RadialAngle = Random.Range(0, 360);
        targetScale = Random.Range(0.6f, 1f);
        swingValueIndex = Random.Range(0, LeafBranchWaver.instance.ValueSampleCount);
        leafBranchHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, RadialAngle, 0));
        leafBranchHolder.transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if(scale < targetScale)
        {
            scale += Time.deltaTime*3;
        }
        else
        {
            scale = targetScale;
        }
        leafBranchHolder.transform.localScale = Vector3.one * scale;

        leafBranchHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, RadialAngle, LeafBranchWaver.instance.swingValues[swingValueIndex] * 5));
    }
}
